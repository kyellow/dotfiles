#!/bin/bash

dconf reset -f /org/gnome/desktop/app-folders/

ENTRIES=$(ls /usr/share/desktop-directories/ | sed -e "s/\(.*\)\.directory.*/\1/")
#ENTRIES=$(grep "<Directory>" /etc/xdg/menus/gnome-applications.menu | grep -v X-GNOME-Menu-Applications.directory | sed -e "s/.*>\(.*\)\.directory.*/\1/")

for ENTRY in $ENTRIES
do
	gsettings set "org.gnome.desktop.app-folders.folder:/org/gnome/desktop/app-folders/folders/$ENTRY/" apps "[]"
	gsettings set "org.gnome.desktop.app-folders.folder:/org/gnome/desktop/app-folders/folders/$ENTRY/" categories "['$ENTRY', 'X-$ENTRY']"
	gsettings set "org.gnome.desktop.app-folders.folder:/org/gnome/desktop/app-folders/folders/$ENTRY/" excluded-apps "[]"
	gsettings set "org.gnome.desktop.app-folders.folder:/org/gnome/desktop/app-folders/folders/$ENTRY/" name "$ENTRY.directory"
	gsettings set "org.gnome.desktop.app-folders.folder:/org/gnome/desktop/app-folders/folders/$ENTRY/" translate true
done

FOLDERS="[$(echo $(for ENTRY in $ENTRIES; do echo -n "'$ENTRY', "; done) | sed -e "s/,*$//g")]"

gsettings set org.gnome.desktop.app-folders folder-children "$FOLDERS"
