#!/bin/bash

cd "$(dirname "${BASH_SOURCE[0]}")" \
	&& source "./utils.sh"

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

function main() {

	print_in_purple "\n  Terminal\n\n"

	execute "gsettings set org.gnome.desktop.interface monospace-font-name 'Monospace 12'" \
		"Change font size"

	execute "gconftool-2 --set '/apps/gnome-terminal/profiles/Default/use_theme_background' --type bool false && \
		gconftool-2 --set '/apps/gnome-terminal/profiles/Default/use_theme_colors' --type bool false && \
		gconftool-2 --set '/apps/gnome-terminal/profiles/Default/palette' --type string \"['rgb(0,0,0)', 'rgb(170,0,0)', 'rgb(0,170,0)', 'rgb(170,85,0)', 'rgb(0,0,170)', 'rgb(170,0,170)', 'rgb(0,170,170)', 'rgb(170,170,170)', 'rgb(85,85,85)', 'rgb(255,85,85)', 'rgb(85,255,85)', 'rgb(255,255,85)', 'rgb(85,85,255)', 'rgb(255,85,255)', 'rgb(85,255,255)', 'rgb(255,255,255)']\" && \
		gconftool-2 --set '/apps/gnome-terminal/profiles/Default/scrollbar-policy' --type string 'never' && \
		gconftool-2 --set '/apps/gnome-terminal/profiles/Default/font' --type string 'Inconsolata Medium 12'" \
		"Set custom terminal theme"

	# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	print_in_purple "\n  UI & UX\n\n"

	execute "gsettings set org.gnome.desktop.background picture-options 'stretched'" \
		"Set desktop background image options"

	execute "gsettings set org.gnome.libgnomekbd.keyboard layouts \"[ 'us', 'tr' ]\"" \
		"Set keyboard languages"

	print_in_purple "\n Icon Theme \n\n"

	execute "mkdir p ~/.icons"
	execute "git clone https://github.com/rudrab/Shadow/ ~/.icons"

}

main
