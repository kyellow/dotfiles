#!/bin/bash

cd "$(dirname "${BASH_SOURCE[0]}")" \
	&& source "./utils.sh"

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

function main() {

	# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	initialize_source_list

	update
	upgrade

	print_in_green "\n  ---\n\n"

	# COMMAND LINE INTERFACE
	# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	# Tools for compiling/building software from source
	install_package "Build Essential" "build-essential"

	# Gnome enhanced vi editor
	install_package "Gnome Vi IMproved" "vim-gnome"

	# distributed source code management tools
	install_package "Git" "git"

	# instrumentation framework for building dynamic analysis tools
	install_package "Vagrind" "valgrind"

	# command-line network traffic analyzer
	install_package "TCPdump" "tcpdump"

	# anti-virus utility for Unix
	install_package "Clam AntiVirüs" "clamav"

	# command line tool for transferring data with URL syntax
	install_package "cURL" "curl"

	# Text-mode WWW Browser
	install_package "Lynx" "lynx"

	# Virtual network computing client software for X
	install_package "XVNC for Viewer" "xvnc4viewer"

	# Adjusts the color temperature of your screen
	install_package "Redshift" "redshift"

	# GnuPG archive keys of the Debian archive
	install_package "GnuPG archive keys" "debian-archive-keyring"

	#  Compiler cache for fast recompilation of C/C++ code
	install_package "ccache" "ccache"
	mkd "~/.ccache"

	print_in_green "\n  ---\n\n"

	# GRAPHICAL USER INTERFACE
	# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	# Web Browser 
	install_package "Chromium" "chromium"

	# multimedia player and streamer
	install_package "VLC" "vlc"

	# x86 virtualization solution
	install_package "VirtualBox" "virtualbox"

	# multimedia player and streamer
	install_package "VLC" "vlc"

	# graphical text editor based on wiki technologies
	install_package "Zim Wiki" "zim"

	# graphical tool to diff and merge files
	install_package "Meld Merge" "meld"

	# Drop-down terminal for GNOME Desktop Environment
	install_package "Guake" "quake"

	# GUI editor for SQLite databases
	install_package "SQLite Browser" "sqlitebrowser"

	# lightweight directory-based password manager
	install_package "Pass" "pass"

	# image manipulation programs
	install_package "ImageMagick" "imagemagick"

	# e-book converter and library management
	install_package "Calibre" "calibre"

	# simple and featureful IRC client for GNOME
	install_package "XChat-GNOME" "xchat-gnome"

	# API documentation browser for GNOME.
	install_package "Devhela" "devhelp"

	# delete unnecessary files from the system
	install_package "Bleachbit" "bleachbit"

	# build tag file indexes of source code definitions
	install_package "CTags" "exuberant-ctags"

	# Flat theme with transparent elements
	install_package "ARC-Theme" "arc-theme"

	# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	print_in_green "\n  ---\n\n"
	update
	upgrade

	# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	print_in_green "\n  ---\n\n"
	autoremove

}

main
