#!/bin/bash

function answer_is_yes() {
	[[ "$REPLY" =~ ^[Yy]$ ]] \
		&& return 0 \
		|| return 1
}

function ask() {
	print_question "$1"
	read -r
}

function ask_for_confirmation() {
	print_question "$1 (y/n) "
	read -r -n 1
	printf "\n"
}

function ask_for_sudo() {

	# Ask for the administrator password upfront
	sudo -v &> /dev/null

	# Update existing `sudo` time stamp until this script has finished
	# https://gist.github.com/cowboy/3118588
	while true; do
		sudo -n true
		sleep 60
		kill -0 "$$" || exit
	done &> /dev/null &

}

function cmd_exists() {
	command -v "$1" &> /dev/null
}

function execute() {

	local tmpFile="$(mktemp /tmp/XXXXX)"
	local exitCode=0

	# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	eval "$1" \
		&> /dev/null \
		2> "$tmpFile"

	print_result $? "${2:-$1}"
	exitCode=$?

	if [ $exitCode -ne 0 ]; then
		print_error_stream "↳ ERROR:" < "$tmpFile"
	fi

	rm -rf "$tmpFile"

	return $exitCode

}

function get_answer() {
	printf "%s" "$REPLY"
}

function get_os_arch() {
	printf "%s" "$(getconf LONG_BIT)"
}

function get_distro_code_name() {
	printf "%s" "$(lsb_release -cs)"
}

function get_distro_name() {
	printf "%s" "$(lsb_release -is)"
}

function is_git_repository() {
	git rev-parse &> /dev/null
}

function mkd() {
	if [ -n "$1" ]; then
		if [ -e "$1" ]; then
			if [ ! -d "$1" ]; then
				print_error "$1 - a file with the same name already exists!"
			else
				print_success "$1"
			fi
		else
			execute "mkdir -p $1" "$1"
		fi
	fi
}

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

function print_error() {
	print_in_red "  [✖] $1 $2\n"
}

function print_error_stream() {
	while read -r line; do
		print_error "$1 $line"
	done
}

function print_in_green() {
	printf "\e[0;32m%b\e[0m" "$1"
}

function print_in_purple() {
	printf "\e[0;35m%b\e[0m" "$1"
}

function print_in_red() {
	printf "\e[0;31m%b\e[0m" "$1"
}

function print_in_yellow() {
	printf "\e[0;33m%b\e[0m" "$1"
}

function print_info() {
	print_in_purple "\n $1\n\n"
}

function print_question() {
	print_in_yellow "  [?] $1"
}

function print_result() {

	if [ "$1" -eq 0 ]; then
		print_success "$2"
	else
		print_error "$2"
	fi

	return "$1"

}

function print_success() {
	print_in_green "  [✔] $1\n"
}

function print_warning() {
	print_in_yellow "  [!] $1\n"
}

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

function initialize_source_list() {

	declare -l DISTRO_NAME=$(get_distro_name)

	declare -r SOURCE_LIST =  "deb http://ftp.debian.org/debian/ $DISTRO_NAME main contrib non-free\n"
	"deb-src http://ftp.debian.org/debian/ $DISTRO_NAME main\n"
	"deb http://security.debian.org/ $DISTRO_NAME/updates main\n"
	"deb-src http://security.debian.org/ $DISTRO_NAME/updates main\n"
	"deb http://ftp.debian.org/debian/ $DISTRO_NAME-updates main\n"
	"deb-src http://ftp.debian.org/debian/ $DISTRO_NAME-updates main\n"
	"deb-src [arch=amd64] https://download.docker.com/linux/debian $DISTRO_NAME stable"

	sudo sh -c "printf '$SOURCE_LIST' >> '/etc/apt/sources.list'"
}

function install_package() {

	declare -r PACKAGE="$2"
	declare -r PACKAGE_READABLE_NAME="$1"

	if ! package_is_installed "$PACKAGE"; then
		execute "sudo apt-get install --allow-unauthenticated -qqy $PACKAGE" "$PACKAGE_READABLE_NAME"
		#                                      suppress output ─┘│
		#            assume "yes" as the answer to all prompts ──┘
	else
		print_success "$PACKAGE_READABLE_NAME"
	fi

}

function package_is_installed() {
	dpkg -s "$1" &> /dev/null
}

function update() {

	# Resynchronize the package index files from their sources

	execute \
		"sudo apt-get update -qqy" \
		#                     │└─  suppress output 
	#            	      └─-  assume "yes" as the answer to all prompts
	"update"

}

function upgrade() {

	# Install the newest versions of all packages installed

	execute \
		"sudo apt-get upgrade -qqy" \
		#                     ││└─ assume "yes" as the answer to all prompt
	#            	      └─ suppress output 
	"upgrade"

}

function autoremove() {

	# Remove packages that were automatically installed to satisfy
	# dependencies for other packages and are no longer needed

	execute \
		"sudo apt-get autoremove -qqy" \
		#                     	  ││└─ assume "yes" as the answer to all prompt
	#            	      	  └─ suppress outpu
	"autoremove"

}
